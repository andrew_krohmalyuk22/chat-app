export default {
  iceServers: [
    {
      urls: 'stun:stun.l.google.com:19302', // Google's public STUN server
    },
    {
      urls: 'stun:stun.services.mozilla.com',
    },
    {
      urls: 'stun:stun.1.google.com:19302',
    },
    {
      urls: 'turn:192.158.29.39:3478?transport=udp',
      credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
      username: '28224511:1379330808',
    },
  ],
};

export const userMediaConfig = {
  video: { width: 822, height: 616 },
  audio: true,
};
