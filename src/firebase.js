import firebase from 'firebase';
import 'firebase/firestore';

const config = {
  apiKey: 'AIzaSyA7pYp12MF2Wo2lW5nquW38FSZos67jLGI',
  authDomain: 'chat-app-d9651.firebaseapp.com',
  databaseURL: 'https://chat-app-d9651.firebaseio.com',
  projectId: 'chat-app-d9651',
  storageBucket: 'chat-app-d9651.appspot.com',
  messagingSenderId: '804640843068',
};
firebase.initializeApp(config);

export const fireStore = firebase.firestore();

fireStore.settings({
  timestampsInSnapshots: true,
});

export default firebase;
