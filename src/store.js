/* eslint-disable no-param-reassign */
import Vue from 'vue';
import Vuex from 'vuex';
import firebase, { fireStore } from '@/firebase';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: null,
    auth: {
      isLoading: false,
    },
    home: {
      users: [],
      activeDialog: null,
      activeUser: null,
      dialogs: [],
      messages: null,
    },
  },
  getters: {
    user: state => state.user,
    messageDialogs: state => state.home.dialogs,
    userContacts: state => (state.user || {}).contacts,
    isAuthLoading: state => state.auth.isLoading,
    activeDialog: state => state.home.activeDialog,
    activeUser: state => state.home.activeUser,
    users: state => state.home.users,
    messages: state => state.home.messages,
  },
  mutations: {
    SET_USER(state, payload) {
      state.user = payload;
    },
    SET_ACTIVE_DIALOG(state, { activeDialog, activeUser }) {
      state.home.activeDialog = activeDialog;
      state.home.activeUser = activeUser;
    },
    SET_AUTH_LOADING_STATE(state, payload) {
      state.auth.isLoading = payload;
    },
    GET_USERS(state, payload) {
      state.home.users = [...payload.users.filter(i => i.userName !== state.user.userName)];
    },
    SET_MESSAGES(state, payload) {
      state.home.messages = [...payload];
    },
    ADD_USER_CONTACT(state, payload) {
      state.user.contacts = [...state.user.contacts, payload];
    },
    REMOVE_USER_CONTACT(state, payload) {
      state.user.contacts = state.user.contacts.filter(item => item !== payload);
    },
    ADD_DIALOG(state, payload) {
      state.user.dialogs = [...state.user.dialogs, payload];
    },
    REMOVE_DIALOG(state, payload) {
      state.user.dialogs = state.user.dialogs.filter(item => item !== payload);
    },
    SET_DIALOGS(state, payload) {
      state.home.dialogs = [...payload];
    },
  },
  actions: {
    setUserData(context, payload) {
      context.commit('SET_USER', payload);
    },
    addUserToContacts(context, payload) {
      context.commit('ADD_USER_CONTACT', payload);
    },
    removeUserFromContacts(context, payload) {
      context.commit('REMOVE_USER_CONTACT', payload);
    },
    setActiveDialog(context, payload) {
      context.commit('SET_ACTIVE_DIALOG', payload);
    },
    setAuthLoadingState(context, payload) {
      context.commit('SET_AUTH_LOADING_STATE', payload);
    },
    setMessages(context, payload) {
      context.commit('SET_MESSAGES', payload);
    },
    addDialog(context, payload) {
      context.commit('ADD_DIALOG', payload);
    },
    removeDialog(context, payload) {
      context.commit('REMOVE_DIALOG', payload);
    },
    async getUsers(context) {
      const users = [];
      await fireStore.collection('users').get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          users.push({
            id: doc.id,
            ...doc.data(),
          });
        });
      });
      context.commit('GET_USERS', { users });
      return Promise.resolve(users[0].id);
    },

    async getDialogs(context) {
      const currentUser = context.state.user;
      const dialogs = await fireStore.collection('dialogs').get()
        .then((querySnapshot) => {
          const result = [];
          querySnapshot.forEach((doc) => {
            result.push({
              ...doc.data(),
            });
          });
          return result;
        });

      const getFriendId = (user, currentUserId) =>
        (user.firstPerson === currentUserId ? user.secondPerson : user.firstPerson);

      const userDialogs = await dialogs
        .filter(dialog => currentUser.dialogs.includes(dialog.id))
        .map(async (i) => {
          const userId = getFriendId(i, currentUser.id);
          const currentUserRef = fireStore.collection('users').doc(userId);
          const { userName, onLine } = (await currentUserRef.get()).data();

          return {
            id: i.id,
            userId,
            userName,
            onLine,
          };
        });
      const result = await Promise.all(userDialogs);
      context.commit('SET_DIALOGS', result);
      context.dispatch('setActiveDialog', {
        activeDialog: (result[0] || {}).id || null,
        activeUser: (result[0] || {}).userName || null,
      });
      context.dispatch('getMessagesByDialog', (result[0] || {}).id);
    },

    async getMessagesByDialog(context, id) {
      if (!id) {
        context.commit('SET_MESSAGES', []);
        return;
      }
      const messages = await fireStore.collection('messages')
        .where('dialogId', '==', id)
        .get()
        .then((querySnapshot) => {
          const result = [];

          querySnapshot.forEach((doc) => {
            result.push({
              id: doc.id,
              ...doc.data(),
            });
          });
          return result;
        });
      context.commit('SET_MESSAGES', messages);
    },
    async getMessages(context) {
      const messages = [];
      await fireStore.collection('messages').get()
        .then((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            messages.push({
              id: doc.id,
              ...doc.data(),
            });
          });
        });
      context.commit('SET_MESSAGES', messages);
    },
    getCurrentUserData(context) {
      const { currentUser } = firebase.auth();
      const currentUserRef = fireStore.collection('users').doc(currentUser.uid);

      currentUserRef.get()
        .then(async (doc) => {
          if (doc.exists) {
            context.commit('SET_USER', doc.data());
            context.dispatch('getDialogs');
          }
        })
        .catch((error) => {
          console.log('Error getting document', error);
          context.commit('SET_USER', null);
        });
    },
  },
  strict: true,
});
