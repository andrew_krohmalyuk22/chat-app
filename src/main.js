import Vue from 'vue';
import 'webrtc-adapter';
import '@/plugins/vuetify';
import '@/plugins/vueChatScroll';
import firebase from './firebase';
import App from './App.vue';
import router from './router';
import store from './store';

const app = '';

Vue.config.productionTip = false;

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    new Vue({
      router,
      store,
      render: h => h(App),
    }).$mount('#app');
  }
});
