import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary: '#FBC02D',
    secondary: '#F9A825',
    accent: '#FBC02D',
    error: '#FF5722',
    warning: '#3f51b5',
    info: '#2196f3',
    success: '#4caf50',
  },
  options: {
    customProperties: true,
  },
});
