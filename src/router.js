import Vue from 'vue';
import Router from 'vue-router';
import firebase from 'firebase';
import Home from './views/Home.vue';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
    {
      path: '/contacts',
      name: 'contacts',
      meta: {
        requiresAuth: true,
      },
      component: () => import(/* webpackChunkName: "about" */ './views/Contacts.vue'),
    },
    {
      path: '/video-room',
      name: 'video-room',
      meta: {
        requiresAuth: true,
        isHideFooter: true,
      },
      component: () => import(/* webpackChunkName: "about" */ './views/VideoRoom.vue'),
    },
    {
      path: '/video-response',
      name: 'video-response',
      meta: {
        requiresAuth: true,
        isHideFooter: true,
      },
      component: () => import(/* webpackChunkName: "about" */ './views/VideoResponse.vue'),
    },
    {
      path: '*',
      name: 'login',
      component: () => import(/* webpackChunkName: "about" */ './views/Login.vue'),
    },
    {
      path: '/sign-up',
      name: 'sign-up',
      component: () => import(/* webpackChunkName: "about" */ './views/SignUp.vue'),
    },
  ],
});

router.beforeEach((to, from, next) => {
  const { currentUser } = firebase.auth();
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if (requiresAuth && !currentUser) next('login');
  else if (!requiresAuth && currentUser) next('home');
  else next();
});

export default router;
