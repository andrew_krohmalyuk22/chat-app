
export default (fun, time) => {
  let timeout;
  return () => {
    clearTimeout(timeout);
    timeout = setTimeout(fun, time);
  };
};
