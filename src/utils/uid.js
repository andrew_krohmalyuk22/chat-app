import UIDGenerator from 'uid-generator';

const uidgen = new UIDGenerator();

export default () => uidgen.generate();
